<?php

namespace Email;

require_once __DIR__ . '/PHPMailer/src/PHPMailer.php';
require_once __DIR__ . '/config.php';

use PHPMailer\PHPMailer\PHPMailer;

class KlinikSender extends PHPMailer
{
    public function __construct($exceptions = null)
    {
        parent::__construct($exceptions);

        $this->SMTPDebug = CONFIG['debug_level'];
        $this->isSMTP();
        $this->SMTPAuth = isset(CONFIG['smtp']['auth']);
        $this->Host = CONFIG['smtp']['host'];
        $this->Username = CONFIG['smtp']['auth']['username'];
        $this->Password = CONFIG['smtp']['auth']['password'];
        $this->SMTPSecure = CONFIG['smtp']['secure'];
        $this->Port = CONFIG['smtp']['port'];
        $this->Subject = CONFIG['letter']['theme'];
        $this->CharSet = CONFIG['letter']['charset'];
        $this->setFrom(CONFIG['sender']['email'], CONFIG['sender']['name']);
        $this->addReplyTo(CONFIG['sender']['email'], CONFIG['sender']['name']);
        $this->setLanguage(CONFIG['letter']['lang']);
        $this->isHTML(CONFIG['letter']['html']);
        $this->addAddress(CONFIG['recepient']);
    }

    private function getPreparedBody(string $name, string $phone, string $date): string
    {
        return sprintf(CONFIG['letter']['body'],
            htmlspecialchars($name),
            htmlspecialchars($phone),
            htmlspecialchars($date)
        );
    }

    public function sendNewRequest(string $name, string $phone, string $date): bool
    {
        $this->Body = $this->getPreparedBody($name, $phone, $date);
        return $this->send();
    }
}
