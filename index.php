<?php

require_once  __DIR__.'/PHPMailer/src/Exception.php';
require_once __DIR__ . '/KlinikSender.php';

use PHPMailer\PHPMailer\Exception;
use Email\KlinikSender;

try {

    $mail = new KlinikSender(true);

    if($mail->sendNewRequest($_POST['fio'] ?? '', $_POST['phone'] ?? '', $_POST['date'] ?? '')) {
        echo 'Message send';
    }else{
        echo 'Message not send';
    }

} catch (Exception $e) {
    echo 'Message not send. Error: '.$mail->ErrorInfo;
}